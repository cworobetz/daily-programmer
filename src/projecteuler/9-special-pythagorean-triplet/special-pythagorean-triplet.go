package main

import "fmt"

/*
A Pythagorean triplet is a set of three natural numbers, a < b < c, for which,

a^2 + b^2 = c^2

For example, 3^2 + 4^2 = 9 + 16 = 25 = 5^2.
There exists exactly one Pythagorean triplet for which a + b + c = 1000.
Find the product abc.
*/

func main() {

	for a := 1; a < 1000; a++ {
		for b := a + 1; b < 1000-a; b++ {
			c := 1000 - b - a

			if c < b || b < a {
				break
			}

			aa := a * a
			bb := b * b
			cc := c * c

			if aa+bb == cc {
				fmt.Printf("Pythagorean triplet found. abc = %d", a*b*c)
			}
		}
	}

}
