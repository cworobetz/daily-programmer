package main

import "fmt"

/*
The prime factors of 13195 are 5, 7, 13 and 29.
What is the largest prime factor of the number 600851475143 ?
*/

func main() {
	var factorable = 600851475143
	var factors []int

	for i := 2; i <= factorable; i++ {
		if factorable%i == 0 {
			factors = append(factors, i)
			factorable /= i
		}
	}
	fmt.Printf("Largest prime factor: %d", factors[len(factors)-1])
}
