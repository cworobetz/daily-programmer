package main

import (
	"fmt"
	"math"
)

func main() {

	i := 1
	primesCounter := 0

	for primesCounter < 10001 {
		if isPrime(i) {
			primesCounter++
		}
		if primesCounter == 10001 {
			fmt.Printf("The 10001st prime is %d", i)
		}
		i++
	}
}

func isPrime(num int) bool {
	for i := 2; i <= int(math.Floor(float64(num)/2)); i++ {
		if num%i == 0 {
			return false
		}
	}
	return num > 1
}
