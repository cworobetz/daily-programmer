package main

import (
	"fmt"
	"strconv"
)

/*
A palindromic number reads the same both ways. The largest palindrome made from the product of two 2-digit numbers is 9009 = 91 × 99.
Find the largest palindrome made from the product of two 3-digit numbers.
*/

func main() {
	largest := 0
	for i := 100; i <= 999; i++ {
		for j := 100; j <= 999; j++ {
			num := i * j
			if checkPalidromeNum(num) && num > largest { // If it's a palindrome and larger than the current largest
				largest = num
			}
		}
	}
	fmt.Printf("The largest palindome is %d\n", largest)
}

func checkPalidromeNum(num int) bool {

	// Compare the normal num to the reversed num. If they're equal, return true
	strnum := strconv.Itoa(num)
	revstrnum := reverse(strnum)

	if strnum == revstrnum {
		return true
	}
	return false
}

func reverse(s string) string {
	chars := []rune(s)
	for i, j := 0, len(chars)-1; i < j; i, j = i+1, j-1 { // This for syntax looks like black magic, but it's just using multiple assignment syntax
		chars[i], chars[j] = chars[j], chars[i]
	}
	return string(chars)
}
