package main

import "fmt"

/*
The sum of the squares of the first ten natural numbers is,
1^2 + 2^2 + ... + 10^2 = 385
The square of the sum of the first ten natural numbers is,
(1 + 2 + ... + 10)^2 = 55^2 = 3025
Hence the difference between the sum of the squares of the first ten natural numbers and the square of the sum is 3025 − 385 = 2640.
Find the difference between the sum of the squares of the first one hundred natural numbers and the square of the sum.
*/

func main() {

	// Grab the sum of adding the squares of nums 1-100
	sumsquares100 := 0
	for i := 1; i <= 100; i++ {
		sumsquares100 += i * i
	}

	// Grab the sum of adding nums 1-100
	sumadd100 := 0
	for i := 1; i <= 100; i++ {
		sumadd100 += i
	}
	// Square it
	squared := sumadd100 * sumadd100

	// difference them for the answer
	answer := squared - sumsquares100

	fmt.Printf("Answer: %d", answer)
}
