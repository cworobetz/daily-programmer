package main

import (
	"fmt"
	"math/big"
)

/*
The sum of the primes below 10 is 2 + 3 + 5 + 7 = 17.
Find the sum of all the primes below two million.
*/

func main() {
	sumprimes := 0
	for i := 0; i < 2000000; i++ {
		n := int64(i)
		if big.NewInt(n).ProbablyPrime(0) {
			sumprimes += i
		}
	}
	fmt.Printf("Sum of all primes below 2,000,000: %d", sumprimes)
}
