package main

import "fmt"

/*
2520 is the smallest number that can be divided by each of the numbers from 1 to 10 without any remainder.
What is the smallest positive number that is evenly divisible by all of the numbers from 1 to 20?
*/

func main() {
	complete := false

	for i := 20; complete != true; i++ {
		for j := 1; j <= 20 && complete != true; j++ {
			if i%j != 0 {
				break
			}
			if j == 20 {
				fmt.Printf("%d is divisible by all numbers 1-20\n", i)
				complete = true
			}
		}
	}

}
