# In the board game 7 Wonders, there are four basic resources, which we'll abbreviate with letters: W for wood, B for
#     brick, S for stone, and O for ore.
# Resource cards let you produce one of your choice of resources. We'll use W/B to represent a resource card that can
#     give you either 1 wood or 1 brick, but not both.
# Given the resource cards W/B/S/O, W, S/B, and S, it is possible for you to produce 2 wood and 2 stone: use the first
#      two cards to get wood, and the last two to get stone. However, with that same set of cards, it is impossible for
#      you to produce 2 wood and 2 brick.

neededresources = ['A', 'A', 'B', 'C', 'C', 'C', 'C', 'C', 'C', 'D', 'D', 'D', 'E', 'E', 'E', 'E', 'F', 'F', 'G', 'G']

resourcecards = ['ABDE', 'ABEFG', 'AD', 'ADE', 'ADE', 'BCDG', 'BCE', 'BCEF', 'BCEF', 'BDE', 'BDE', 'BEF', 'CDF', 'CE',
                 'CEFG', 'CF', 'CF', 'DEFG', 'DF', 'EG']


def check(permutation, neededresources):
    i = 0
    for card in permutation:
        if neededresources[i] in card:
            i = i + 1
            if i >= len(neededresources):
                return True
            continue
        else:
            return False


possible = False
for permutation in resourcepermuations:
    if check(permutation, neededresources):
        possible = permutation
        break

if possible:
    print(f"It's possible for the resource card permutation {permutation} checking for {neededresources}!")
else:
    print("It's not possible")
