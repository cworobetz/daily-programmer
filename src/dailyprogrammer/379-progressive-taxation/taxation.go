package main

import (
	"bufio"
	"fmt"
	"log"
	"os"
	"regexp"
	"strconv"
	"strings"
)

func tax(income int) int {
	var tax int

	if income > 100000 {
		tax += 19500
		var excess = income - 100000
		tax += int(float32(excess) * .4)
	}

	if income > 30000 && income <= 100000 {
		tax += 2000
		var excess = income - 30000
		tax += int(float32(excess) * .25)
	}

	if income > 10000 && income <= 30000 {
		var excess = income - 10000
		tax += int(float32(excess) * .1)
	}
	return tax
}

func main() {
	fmt.Print("Please enter your years income: ")
	reader := bufio.NewReader(os.Stdin)
	input, _ := reader.ReadString('\n')
	input = strings.TrimSuffix(input, "\n")

	// Validate that the input is an integer
	regex := `^(\d)+$`
	expression := regexp.MustCompile(regex)
	if !expression.MatchString(input) {
		log.Fatal("Input does not match regex \"^(\\d)+$\"")
	}

	income, _ := strconv.Atoi(input)

	taxdue := tax(income)

	fmt.Printf("You owe $%d.\n", taxdue)
}
