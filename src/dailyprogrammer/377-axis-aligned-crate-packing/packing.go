package main

import (
	"fmt"
)

type square struct {
	x int
	y int
}

type cube struct {
	x int
	y int
	z int
}

func main() {

	fmt.Println("fit1 (Square crate, square boxes, no rotations")
	fmt.Println(fit1(square{25, 18}, square{6, 5}))
	fmt.Println(fit1(square{10, 10}, square{1, 1}))
	fmt.Println(fit1(square{12, 34}, square{5, 6}))
	fmt.Println(fit1(square{12345, 678910}, square{1112, 1314}))
	fmt.Println(fit1(square{5, 100}, square{6, 1}))

	fmt.Println("fit2 (Square crate, square boxes, 90 degree rotations allowed. All boxes always have same orientation")
	fmt.Println(fit2(square{25, 18}, square{6, 5}))
	fmt.Println(fit2(square{12, 34}, square{5, 6}))
	fmt.Println(fit2(square{12345, 678910}, square{1112, 1314}))
	fmt.Println(fit2(square{5, 5}, square{3, 2}))
	fmt.Println(fit2(square{5, 100}, square{6, 1}))
	fmt.Println(fit2(square{5, 5}, square{6, 1}))

	fmt.Println("fit3 (Cube crate, cube boxes, 90 degree rotations on any axis allowed. All boxes always have same orientation")
	fmt.Println(fit3(cube{10, 10, 10}, cube{1, 1, 1}))
	fmt.Println(fit3(cube{12, 34, 56}, cube{7, 8, 9}))
	fmt.Println(fit3(cube{123, 456, 789}, cube{10, 11, 12}))
	fmt.Println(fit3(cube{1234567, 89101112, 13141516}, cube{171819, 202122, 232425}))
}

func fit1(cratesize square, squaresize square) int {

	xfits := cratesize.x / squaresize.x
	yfits := cratesize.y / squaresize.y

	numsquarees := xfits * yfits

	return numsquarees
}

func fit2(cratesize square, squaresize square) int {
	normal := fit1(cratesize, squaresize)
	rotated := fit1(cratesize, square{squaresize.y, squaresize.x})

	if rotated >= normal {
		return rotated
	}

	return normal
}

func fit3(cratesize cube, cubesize cube) int {

	var numfits = func(cratesize cube, cubex int, cubey int, cubez int) int {
		xfits := cratesize.x / cubex
		yfits := cratesize.y / cubey
		zfits := cratesize.z / cubez
		return xfits * yfits * zfits
	}

	// Try all possible different cube orientations
	fits1 := numfits(cratesize, cubesize.x, cubesize.y, cubesize.z)
	fits2 := numfits(cratesize, cubesize.x, cubesize.z, cubesize.y)
	fits3 := numfits(cratesize, cubesize.y, cubesize.x, cubesize.z)
	fits4 := numfits(cratesize, cubesize.y, cubesize.z, cubesize.x)
	fits5 := numfits(cratesize, cubesize.z, cubesize.x, cubesize.y)
	fits6 := numfits(cratesize, cubesize.z, cubesize.y, cubesize.x)

	returnvals := [6]int{fits1, fits2, fits3, fits4, fits5, fits6}
	max := returnvals[0]
	for _, value := range returnvals {
		if value > max {
			max = value
		}
	}

	return max
}
