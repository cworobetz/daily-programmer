package main

import "fmt"

func main() {
	inputs := []string{"xxxyyy", "yyyxxx", "xxxyyyy", "yyxyxxyxxyyyyxxxyxyx", "xyxxxxyyyxyxxyxxyy", "", "x", "xxxyyyzzz", "abccbaabccba", "xxxyyyzzzz", "abcdefghijklmnopqrstuvwxyz", "pqq", "fdedfdeffeddefeeeefddf", "www", "x", ""}

	// Main loop, over all the inputs above
MAIN:
	for _, input := range inputs {

		// Check the null case
		if len(input) == 0 {
			fmt.Printf("\"%s\" is empty, therefore balanced.\n", input)
			continue
		}

		// Holds a key-value maps, in essence something like: [['x', 3], ['y', 3]]
		keycount := make(map[rune]int)
		count := 0

		// Loop over every character. Update the map
		for _, char := range input {
			keycount[char]++
			count = keycount[char]
		}

		// Check the map. If all values are equal, it's balanced
		for _, v := range keycount {
			if v != count {
				fmt.Printf("\"%s\" is not balanced. Map: %v\n", input, keycount)
				continue MAIN
			}
		}
		fmt.Printf("\"%s\" is balanced. Map: %v\n", input, keycount)
	}
}
