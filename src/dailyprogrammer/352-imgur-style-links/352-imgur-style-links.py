# Short links have been all the rage for several years now, spurred in part by Twitter's character limits.
#     Imgur - Reddit's go-to image hosting site - uses a similar style for their links. Monotonically increasing IDs
#     represented in Base62.
# Your task today is to convert a number to its Base62 representation.

import math


class Base62():
    def __init__(self):
        self.units = "0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"

    def convertbase10(self, number):
        b62conversion = ""
        while True:
            if number is 0:
                break
            modulo = number % 62
            number = math.floor(number / 62)
            b62conversion = self.units[modulo] + b62conversion

        return b62conversion


b62 = Base62()

print(f"Expected: MO9, {b62.convertbase10(187621)}")
print(f"Expected: g62n3, {b62.convertbase10(237860461)}")
print(f"Expected: 9b4B, {b62.convertbase10(2187521)}")
