# In theoretical computer science, the closest string is an NP-hard computational problem, which tries to find the
# geometrical center of a set of input strings. To understand the word "center", it is necessary to define a distance
#  between two strings. Usually, this problem is studied with the Hamming distance in mind. This center must be one of
# the input strings.

# In bioinformatics, the closest string problem is an intensively studied facet of the problem of finding signals in
# DNA. In keeping with the bioinformatics utility, we'll use DNA sequences as examples.

# Consider the following DNA sequences:

# ATCAATATCAA
# ATTAAATAACT
# AATCCTTAAAC
# CTACTTTCTTT
# TCCCATCCTTT
# ACTTCAATATA

# Using the Hamming distance (the number of different characters between two sequences of the same length),
#  the all-pairs distances of the above 6 sequences puts ATTAAATAACT at the center.

input = """21
ACAAAATCCTATCAAAAACTACCATACCAAT
ACTATACTTCTAATATCATTCATTACACTTT
TTAACTCCCATTATATATTATTAATTTACCC
CCAACATACTAAACTTATTTTTTAACTACCA
TTCTAAACATTACTCCTACACCTACATACCT
ATCATCAATTACCTAATAATTCCCAATTTAT
TCCCTAATCATACCATTTTACACTCAAAAAC
AATTCAAACTTTACACACCCCTCTCATCATC
CTCCATCTTATCATATAATAAACCAAATTTA
AAAAATCCATCATTTTTTAATTCCATTCCTT
CCACTCCAAACACAAAATTATTACAATAACA
ATATTTACTCACACAAACAATTACCATCACA
TTCAAATACAAATCTCAAAATCACCTTATTT
TCCTTTAACAACTTCCCTTATCTATCTATTC
CATCCATCCCAAAACTCTCACACATAACAAC
ATTACTTATACAAAATAACTACTCCCCAATA
TATATTTTAACCACTTACCAAAATCTCTACT
TCTTTTATATCCATAAATCCAACAACTCCTA
CTCTCAAACATATATTTCTATAACTCTTATC
ACAAATAATAAAACATCCATTTCATTCATAA
CACCACCAAACCTTATAATCCCCAACCACAC"""

lines = input.split('\n')[1:]
linesdict = dict.fromkeys(lines)

for comparetoline in lines:  # For every line
    for comparefromline in lines:  # And for every other line
        if comparetoline == comparefromline:  # Excluding itself
            continue
        for ctl, cfl in zip(comparetoline, comparefromline):
            if ctl != cfl:
                if linesdict[comparetoline] is None:
                    linesdict[comparetoline] = 0
                linesdict[comparetoline] += 1  # If they're not equal, increase the hamming distance

print(min(linesdict, key=linesdict.get))
