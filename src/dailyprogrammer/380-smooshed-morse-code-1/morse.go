package main

import (
	"bufio"
	"fmt"
	"os"
	"strings"
	"regexp"
	"log"
)

func smorse(alphatext string) {
	rosettatext := ".- -... -.-. -.. . ..-. --. .... .. .--- -.- .-.. -- -. --- .--. --.- .-. ... - ..- ...- .-- -..- -.-- --.."

	for i := 0; i < len(alphatext); i++ {
		fmt.Print(strings.Split(rosettatext, " ")[alphatext[i]-97])
	}
	fmt.Println()
}

func main() {
	fmt.Print("Please enter a lowercase alpha string to translate: ")
	reader := bufio.NewReader(os.Stdin)
	input, _ := reader.ReadString('\n')
	input = strings.TrimSuffix(input, "\n")

	// Validate that it's lowercase alphabetical characters
	regex := `^[a-z]+$`
	expression := regexp.MustCompile(regex)
	matches := expression.MatchString(input)
	if !matches {
		log.Fatal("Input does not match regex '$[a-z]*^")
	}

	smorse(input)
}