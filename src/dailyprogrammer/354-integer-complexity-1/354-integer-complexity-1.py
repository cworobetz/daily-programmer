# Given a number A, find the smallest possible value of B+C, if B*C = A. Here A, B, and C must all be positive integers.
# It's okay to use brute force by checking every possible value of B and C. You don't need to handle inputs larger
# than six digits. Post the return value for A = 345678 along with your solution.

# For instance, given A = 12345 you should return 838. Here's why. There are four different ways to represent 12345
# as the product of two positive integers:

# 12345 = 1*12345
# 12345 = 3*4115
# 12345 = 5*2469
# 12345 = 15*823

# The sum of the two factors in each case is:

# 1*12345 => 1+12345 = 12346
# 3*4115 => 3+4155 = 4158
# 5*2469 => 5+2469 = 2474
# 15*823 => 15+823 = 838

# The smallest sum of a pair of factors in this case is 838.

from functools import reduce
from math import ceil


# Thanks, Stack overflow, for this wonderful function
def factors(number):
    return sorted(
        set(reduce(list.__add__, ([i, number // i] for i in range(1, int(pow(number, 0.5) + 1)) if number % i == 0))))


a = 1234567891011
afactors = factors(a)
iterations = ceil(len(afactors) / 2)

lowest = a
for i in range(iterations):  # Compare two ends of the afactors list
    if afactors[i] + afactors[-i - 1] < lowest:
        lowest = afactors[i] + afactors[-i - 1]

print(lowest)
