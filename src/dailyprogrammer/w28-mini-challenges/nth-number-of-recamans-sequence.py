# 0, 1, 3, 6, 2, 7, 13, 20, 12...

# a(0) = 0; for n > 0, a(n) = a(n-1) - 1 if positive and not already in the sequence, otherwise a(n) = a(n-1) + n

def recaman(n):
    prev = [0]
    for i in range(n + 1):
        if prev[-1] - i not in prev and prev[-1] - i > 0:
            prev.append(prev[-1] - i)
        else:
            prev.append(prev[-1] + i)
    return prev[-1]


for i in [5, 15, 25, 100, 1005]:
    print(recaman(i))
