package main

import (
	"fmt"
	"strconv"
	"strings"
)

func main() {
	inputs := `13
1234
9876
199`

	for _, input := range strings.Split(inputs, "\n") { // One line at a time
		fmt.Printf("Input: %v\n", input)
		integer, _ := strconv.Atoi(input) // Convert the string e.g. "13" to an int e.g. 13
		iters := 0                        // Count how many iterations it takes
		done := 0
		for done < 1 {
			iters++
			result := add(integer)                 // Save the result of adding the digits, e.g. 1 + 3 == 4
			if iterativeDigitsCount(result) == 1 { // If the number of digits in the resulting number is one, we're done
				fmt.Printf("Additive persistence count: %d\n", iters)
				done++
			} else {
				integer = result
			}
		}
	}
}

// Adds the individual digits together
func add(integer int) int {
	sum := 0
	for i := 0; i < iterativeDigitsCount(integer); i++ {
		sum += digit(integer, i)
	}
	return sum
}

// Returns the number of digits in an integer
func iterativeDigitsCount(number int) int {
	count := 0
	for number != 0 {
		number /= 10
		count++
	}
	return count
}

// Given an integer and a place, return the integer at the specified place (0-based)
func digit(num int, place int) int {
	str := strconv.Itoa(num)
	intatplace := int(str[place] - '0')
	return intatplace
}
