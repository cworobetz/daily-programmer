import copy

# Things that are needed:
#     - Time
#     - A grid of the size specified
#     - Which grid coordinate needs to be filled

welldesign = """3 3
1 9 6
2 8 5
3 7 4
4"""

gridsize = int(welldesign[0])
time = 0
goal = welldesign[-1]

# Depth grid will have the grid as above
depthgrid = [[0 for i in range(gridsize)] for j in range(gridsize)]  # Initialize a gridsize grid
# Filled grid will have the current depth of the grid, gradually increading as time goes on
filledgrid = copy.deepcopy(depthgrid)

# Initialize grid
for row in range(gridsize):
    for column in range(gridsize):
        # This is probably the most beautiful and ugly thing I've ever written, and it works so well
        # Take that big ugly welldesign string, take only the middle lines, and split it on newlines.
        # You're now iteraring over 3 rows. Over those 3 rows, split it off of spaces. You're now working directly
        # with the numbers. Assign grid[row][column] to the appropriate number, based off the loop. Cast to integer
        depthgrid[row][column] = int(welldesign.split('\n')[1:-1][row].split(' ')[column])

while True:  # filling: filling is equal the number of squares being filled this turn.
